import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        // appBar: AppBar(),
        // body: Column(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: [
        //     Row(
        //       mainAxisAlignment: MainAxisAlignment.end,
        //       children: [
        //         Container(
        //           width: 100,
        //           height: 100,
        //           color: Colors.yellow,
        //         ),
        //         Container(
        //           width: 100,
        //           height: 100,
        //           color: Colors.red,
        //         ),
        //         Container(
        //           width: 100,
        //           height: 100,
        //           color: Colors.blue,
        //         ),
        //       ],
        //     ),
        //     Row(
        //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //       children: [
        //         Container(
        //           width: 100,
        //           height: 100,
        //           color: Colors.yellow,
        //         ),
        //         Container(
        //           width: 100,
        //           height: 100,
        //           color: Colors.red,
        //         ),
        //         Container(
        //           width: 100,
        //           height: 100,
        //           color: Colors.blue,
        //         ),
        //       ],
        //     ),
        //     Row(
        //       children: [
        //         Container(
        //           width: 100,
        //           height: 100,
        //           color: Colors.yellow,
        //         ),
        //         Container(
        //           width: 100,
        //           height: 100,
        //           color: Colors.red,
        //         ),
        //         Container(
        //           width: 100,
        //           height: 100,
        //           color: Colors.blue,
        //         ),
        //       ],
        //     ),
        //   ],
        // ),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.blue,
                Colors.deepPurple
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight
            )
          ),
          child: SafeArea(
              child: Column(
                children: [
                  CircleAvatar(
                    radius: 75,
                    backgroundColor: Colors.grey,
                  ),
                  Text('Ameer Abo AlShar',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.bold
                  ),
                  ),
                  Text('Flutter Developer',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  SizedBox(height: 20,),
                  info(Icon(Icons.phone),'+963 934 801 138'),
                  info(Icon(Icons.email),'ameer@gmail.com'),
                  info(Icon(Icons.location_on),'Damascus'),
                ],
              )
          ),
        ),
      ),
    );
  }

  Widget info(Widget icon,String text){
    return Container(
      width: 300,
      height: 60,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      padding: EdgeInsets.only(left: 10),
      margin: EdgeInsets.all(5),
      child: Row(
        children: [
          icon,
          SizedBox(width: 10,),
          Text(text)
        ],
      ),
    );
  }


}
